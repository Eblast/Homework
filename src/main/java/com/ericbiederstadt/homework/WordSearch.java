package com.ericbiederstadt.homework;

import com.gargoylesoftware.htmlunit.BrowserVersion;
import com.gargoylesoftware.htmlunit.WebClient;
import com.gargoylesoftware.htmlunit.html.HtmlPage;

import java.io.IOException;
import java.net.MalformedURLException;
import java.util.Scanner;
import java.util.stream.Stream;

public class WordSearch {

    public static void main(String[] args) {

        System.out.print("Enter url to search: ");
        Scanner read = new Scanner(System.in);
        //for URL to work in IntelliJ console you have enter a space after you paste in a URL
        String url = read.nextLine().trim();
        System.out.println("\n ");
        System.out.print("Enter word to search for: ");
        String searchTerm = read.nextLine().trim();

        WebClient browser = new WebClient(BrowserVersion.CHROME);
        browser.getOptions().setJavaScriptEnabled(false);

        HtmlPage reddit = null;
        try {
            reddit = browser.getPage(url);

        } catch (IOException e) {
            System.out.println("YOU DONE MESSED UP");
        }
        String content = reddit.asText();

        Stream.of(content.split("\n"))
                .filter(line -> line.contains(searchTerm))
                .map(matched -> "ALERT: " + matched)
                .forEach(System.out::println);



    }

}