package com.ericbiederstadt.homework;

import java.util.Scanner;

public class StringReversal {

    public static void main(String[] args) {
        System.out.print("Enter string to reverse: ");
        Scanner read = new Scanner(System.in);
        String input = read.nextLine();

        char[] characters = input.toCharArray();
        for (int i = 0; i <= (input.length() - 1) / 2; i++) {
            char temp = characters[i];
            characters[i] = characters[input.length() - 1 - i];
            characters[input.length() - 1 - i] = temp;
        }
        System.out.println("Reversed string is: " + String.copyValueOf(characters));
    }
}
